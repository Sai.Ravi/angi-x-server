import json

from flask import Flask, request, jsonify, Blueprint, abort
from werkzeug.exceptions import HTTPException

from app.services.AngiXService import AngiXService

app = Flask(__name__)
service = AngiXService()
angi_x = Blueprint('angi_x', __name__)


@angi_x.route('/api/session', methods=['GET'])
def get_session():
    try:
        session_data = service.initiate_session()
        return jsonify(session_data)
    except Exception:
        abort(500)


@angi_x.route('/api/session/<session_id>/prompt', methods=['POST'])
def post_prompt(session_id):
    text = request.get_json().get('text')
    next_resource_id = service.manage_session_post(session_id, text)
    return jsonify({"next_resource_id": next_resource_id})


@angi_x.route('/api/session/<session_id>/prompt/<prompt_id>', methods=['GET'])
def get_prompt(session_id, prompt_id):
    prompt = service.get_session_prompt(session_id, prompt_id)
    return jsonify(prompt)


@app.errorhandler(HTTPException)
def handle_exception(e):
    """Return JSON instead of HTML for HTTP errors."""
    # start with the correct headers and status code from the error
    response = e.get_response()
    # replace the body with JSON
    response.data = json.dumps({
        "code": e.code,
        "name": e.name,
        "description": e.description,
    })
    response.content_type = "application/json"
    return response


if __name__ == "__main__":
    app.run(debug=True)
