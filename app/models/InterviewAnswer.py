class InterviewAnswer:

    def set_id(self, v):
        self._id = v

    def get_id(self):
        return self._id

    def set_answerId(self, v):
        self._answerId = v

    def get_answerId(self):
        return self._answerId

    def set_answerText(self, v):
        self._answerText = v

    def get_answerText(self):
        return self._answerText

    def set_presentationType(self, v):
        self._presentationType = v

    def get_presentationType(self):
        return self._presentationType

    def set_customer_answer(self, v):
        self._customer_answer = v

    def get_customer_answer(self):
        return self._customer_answer

    # Properties
    id = property(get_id, set_id)
    answerId = property(get_answerId, set_answerId)
    answerText = property(get_answerText, set_answerText)
    presentationType = property(get_presentationType, set_presentationType)
    customer_answer = property(get_customer_answer, set_customer_answer)

    # Init method
    def __init__(self):
        self._id = ""
        self._answerId = ""
        self._answerText = ""
        self._presentationType = ""
        self._customer_answer = ""

    def toJson(self):
        return f'{{"id": {self._id}, "answerId": {self._answerId}, "answerText": "{self._answerText}", "presentationType": "{self._presentationType}", "customer_answer": {self._customer_answer} }}'
