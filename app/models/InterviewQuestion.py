class InterviewQuestion:

    # id
    def set_id(self, v):
        self._id = v

    def get_id(self):
        return self._id

    # question
    def set_question(self, v):
        self._question = v

    def get_question(self):
        return self._question

    # required
    def set_required(self, v):
        self._required = v

    def get_required(self):
        return self._required

    # question type
    def set_question_type(self, v):
        self._question_type = v

    def get_question_type(self):
        return self._question_type

    # answers
    def set_answers(self, v):
        self._answers = v

    def get_answers(self):
        return self._answers

    # Properties
    id = property(get_id, set_id)
    question = property(get_question, set_question)
    required = property(get_required, set_required)
    question_type = property(get_question_type, set_question_type)
    answers = property(get_answers, set_answers)

    # Init method
    def __init__(self):
        self._id = ""
        self._question = ""
        self._required = ""
        self._question_type = ""
        self._answers = ""

    def to_json(self):
        answers = []
        if self._answers is not None:
            for answer in self._answers:
                answers.append(answer.toJson())
        return f'{{"id": {self._id}, "question": "{self._question}", "required": "{self._required}", "questionType": "{self._question_type}", "answers": [{",".join(answers)}] }}'
