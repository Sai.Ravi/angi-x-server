from enum import Enum


class SessionState(Enum):
    LANDING = 1
    INITIAL_TASK_DETECTION_QA = 2
    TASK_QA = 3
    FINAL_TASK_DETECTION_QA = 2
    PI_DATA_QA = 4
    RESULTS_GENERATION = 5
