import os
import requests
import json


def get_api_auth_param_string():
    r_username = os.getenv("R_USERNAME")
    r_access_key = os.getenv("R_ACCESSKEY")
    entity_id = os.getenv("ENTITY_ID")
    entity_hash = os.getenv("ENTITY_HASH")
    return f'r_username={r_username}&r_accesskey={r_access_key}&entityId={entity_id}&entityHash={entity_hash}'


def get_category_interview_detail(category_id):
    url = f'https://www.homeadvisor.com/api/resource/category/interview/-{category_id}/detail?{get_api_auth_param_string()}'
    req = requests.get(url)
    return json.loads(req.content)


def get_sr_qna_task_interview_sets(task_id):
    url = f'https://www.homeadvisor.com/api/resource/sr/qna/{task_id}/interview/3/sets?{get_api_auth_param_string()}'
    req = requests.get(url)
    return json.loads(req.content)


def get_sr_qna_sets_task_interview(interview_id):
    url = f'https://www.homeadvisor.com/api/resource/sr/qna/sets/{interview_id}/interview?{get_api_auth_param_string()}'
    req = requests.get(url)
    return json.loads(req.content)


def post_sr_interview_cat_task_interview():
    raise NotImplementedError


def post_sr_interview_cat_task_interview_answer():
    raise NotImplementedError


def get_sr_interview_cat_task_interview_card():
    raise NotImplementedError


def get_sr_interview_cat_task_interview_qa_results():
    raise NotImplementedError


def get_best_match_matching_source_version():
    raise NotImplementedError


def post_consumer_register():
    raise NotImplementedError


def post_service_request():
    raise NotImplementedError


def post_best_match_decision_combined():
    raise NotImplementedError


def get_best_match_decision():
    raise NotImplementedError
