import os
import pandas as pd
from app.utils.AngiApiUtils import get_category_interview_detail, get_sr_qna_task_interview_sets, \
    get_sr_qna_sets_task_interview
from app.models.InterviewQuestion import QA
from app.models.InterviewAnswer import Answer


def get_category_qa_data(category_name):
    category_id = get_category_task_id_by_name(category_name)
    category_detail = get_category_interview_detail(category_id)
    return process_cat_qa_set(category_detail)

def load_tasks_data():
    task_data_file_path = os.path.join('app', 'content', 'CatTaskData.csv')
    return load_csv_file(task_data_file_path)


def load_categories_data():
    raise NotImplementedError


def load_category_task_mapping_data():
    raise NotImplementedError


def process_cat_task_data():
    raise NotImplementedError


def get_task_qa_data(task_name):
    task_id = get_category_task_id_by_name(task_name)
    qa_messages = []
    interviews = get_sr_qna_task_interview_sets(task_id)
    for interview in interviews:
        if interview is not None:
            interview_id = interview['id']
            interview_qa = get_sr_qna_sets_task_interview(interview_id)
            for qa_message in interview_qa:
                set_interview_qa = process_task_qa_set(qa_message)
                qa_messages.append(set_interview_qa)
    return qa_messages


def get_category_task_id_by_name(name):
    task_data = load_tasks_data()
    for index in task_data.index:
        if task_data["task_description"][index] == name:
            return task_data["taskoid"][index]
    return None


def process_cat_qa_set(category_json):
    qa_message = QA()
    qa_message.id = category_json["questionID"]
    qa_message.question = category_json["questionText"]
    qa_message.required = ""
    qa_message.question_type = ""

    qa_message.answers = []
    for answer in category_json["answerElements"]:
        qa_message_answer = Answer()
        qa_message_answer.id = answer["answerID"]
        qa_message_answer.answerText = answer["answerText"]
        qa_message.answers.append(qa_message_answer)
    return qa_message


def process_task_qa_set(interview_json):
    qa_message = QA()
    qa_message.id = interview_json["questionId"]
    qa_message.question = interview_json["questionText"]
    qa_message.required = interview_json["required"]
    qa_message.question_type = interview_json["questionType"]

    qa_message.answers = []
    for answer in interview_json["answers"]:
        qa_message_answer = Answer()
        qa_message_answer.id = answer["answerId"]
        qa_message_answer.answerText = answer["answerText"]
        qa_message.answers.append(qa_message_answer)
    return qa_message


def load_csv_file(file_path):
    # Load CSV file and return the dataframe
    return pd.read_csv(file_path)
