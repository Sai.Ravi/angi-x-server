from app.services.LLMChatService import LLMChatService
from app.utils.ChatPromptTemplates import TASK_DETECTION_MATCH_CHAT_TEMPLATE
from app.utils.PromptStatus import PromptStatus


class TaskDetectionService:
    def __init__(self, llm_chat_service: LLMChatService):
        self.TASK_DETECTION_MAX_TOKEN = 300

        self.initial_session_processed_dict = dict()
        self.final_session_processed_dict = dict()
        self.llm_chat_service = llm_chat_service

    def generate_task_detection_confidence_chat_prompt(self):
        raise NotImplementedError

    def generate_task_detection_followup_chat_prompt(self):
        raise NotImplementedError

    def generate_task_detection_match_chat_prompt(self, session_id, customer_prompt):
        prompt = TASK_DETECTION_MATCH_CHAT_TEMPLATE.substitute(customer_prompt=customer_prompt)
        self.llm_chat_service.add_session_prompt(session_id, {"role": "system", "content": prompt.strip()})

    def generate_task_detection_text_angix_prompt(self):
        raise NotImplementedError

    def generate_task_detection_text_end_angix_prompt(self):
        raise NotImplementedError

    def generate_task_detection_template_end_angix_prompt(self):
        raise NotImplementedError

    def manage_task_detection(self, session_id, customer_prompt):
        # Initialize session processing for initial and final task detection
        if session_id not in self.initial_session_processed_dict:
            self.initial_session_processed_dict[session_id] = False
        if session_id not in self.final_session_processed_dict:
            self.final_session_processed_dict[session_id] = False

        self.generate_task_detection_match_chat_prompt(session_id, customer_prompt)
        self.llm_chat_service.post_session_prompts_to_chatgpt_model(session_id, self.TASK_DETECTION_MAX_TOKEN)

        # Task Detection for session is processed at this point
        self.initial_session_processed_dict[session_id] = True

        return [{'prompt_text': self.llm_chat_service.get_session_prompt_last(session_id),
                 'prompt_status': PromptStatus.TASK_DETECTION_TEXT_END}]

    def session_processed(self, session_id, initial_round):
        if initial_round:
            return self.initial_session_processed_dict[session_id]
        else:
            return self.final_session_processed_dict[session_id]

