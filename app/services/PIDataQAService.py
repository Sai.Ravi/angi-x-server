import os

from app.utils.PromptStatus import PromptStatus
from app.utils.ChatPromptTemplates import PII_DETECTION_MATCH_CHAT_TEMPLATE


class PIDataQAService:
    def __init__(self, llm_chat_service):
        self.PI_DATA_QA_RESPONSE_MAX_TOKEN = 3
        self.session_processed_dict = dict()
        self.pi_data = {
            'first_name': None,
            'last_name': None,
            'phone': None,
            'email': None,
            'zip': None
        }
        self.llm_chat_service = llm_chat_service

    def generate_pi_data_chat_prompt(self, customer_prompt, session_id):
        # Processing customer_prompt and updating pi_data
        if customer_prompt != "":
            customer_data = self.parse_customer_prompt(customer_prompt, session_id)
            for key in self.pi_data.keys():
                if key in customer_data:
                    self.pi_data[key] = customer_data[key]

        # Generating chat prompt
        first_response = True
        pii_data_string = ""
        for item in self.pi_data:
            if self.pi_data[item] is not None:
                first_response = False
                pii_data_string += f"<{item}>: {self.pi_data[item]}\n"
            else:
                pii_data_string += f"<{item}>: Unknown\n"
        prompt = PII_DETECTION_MATCH_CHAT_TEMPLATE.substitute(pii_data_string=pii_data_string)

        missing_data = [key for key, value in self.pi_data.items() if value is None]
        if first_response or missing_data is not True:
            prompt = self.generate_pi_data_text_angix(missing_data)

        # Send chat prompt to ChatGPT via LLMChatService
        self.llm_chat_service.add_session_prompt(session_id, {"role": "system", "content": prompt})
        self.llm_chat_service.post_session_prompts_to_chatgpt_model(session_id)
        return prompt

    def generate_pi_data_text_angix(self, missing_data):
        if missing_data:
            return f"Thanks for providing the information. We're almost there in matching you with the top-rated pros " \
                   f"in your area! " \
                   f"To finalize the process, we just need a bit more information from you: {', '.join(missing_data)}. " \
                   f"Rest assured, Angi will only share your personal details with the pros you choose."
        else:
            return "Great! We now have all the necessary information. Thank you for completing the required details. " \
                   f"Our team will now work on matching you with the best professionals in your area. "

    def generate_pi_data_template_angix(self):
        template = {}
        for item in self.pi_data:
            if self.pi_data[item] is not None:
                template[item] = self.pi_data[item]
            else:
                template[item] = 'Unknown'
        return template

    def process_assistant_prompt_chat(self, session_id):
        # Get ChatGPT response
        last_assistant_prompt = self.llm_chat_service.get_session_prompt_last(session_id)

        return last_assistant_prompt

    def parse_customer_prompt(self, prompt, session_id):
        pi_data = {'first_name': self.extract_entity(prompt, 'first_name', session_id),
                   'last_name': self.extract_entity(prompt, 'last_name', session_id),
                   'phone': self.extract_entity(prompt, 'phone', session_id),
                   'email': self.extract_entity(prompt, 'email', session_id),
                   'zip': self.extract_entity(prompt, 'zip', session_id)}

        return pi_data

    def extract_entity(self, prompt, entity, session_id):
        extract_template = f"Extract {entity} from the following prompt after the text \"Thanks for providing the " \
                      f"information\" if its there - otherwise, reply with \"No\". Only reply in this format if {entity} is in the " \
                      f"prompt: \"Yes, <{entity}_value>\". Otherwise, only reply with \"No\". Here is the prompt that " \
                      f"may or may not contain the data you need: \"{prompt}. {self.llm_chat_service.get_session_prompt_all(session_id)}\""
        extract_prompt = {"role": "user", "content": extract_template}

        self.llm_chat_service.add_session_prompt(session_id, extract_prompt)
        self.llm_chat_service.post_session_prompts_to_chatgpt_model(session_id, self.PI_DATA_QA_RESPONSE_MAX_TOKEN)
        assistant_response = self.llm_chat_service.get_session_prompt_last(session_id)

        # TODO This should not be needed anymore as PI_DATA_QA_RESPONSE_MAX_TOKEN is set to 3 and it should be one word
        # TODO Remove after testing
        if "No" not in assistant_response:
            entity = assistant_response.split("Yes, ")[1].strip()[:-1]
            return entity
        return None

    def manage_pi_data_qa(self, session_id, customer_prompt=""):
        # Initialize session processing
        if session_id not in self.session_processed_dict:
            self.session_processed_dict[session_id] = False

        self.llm_chat_service.initiate_chat_session(session_id, os.path.join('config.json'))
        # Step 1: Generate chat prompt and send it to ChatGPT via LLMChatService
        angix_prompt = self.generate_pi_data_chat_prompt(customer_prompt, session_id)

        # Step 2: Process ChatGPT response
        last_assistant_prompt = self.process_assistant_prompt_chat(session_id) #make sure it was processed

        prompt_status = PromptStatus.TASK_PI_TEXT if "Thanks for providing the information" in angix_prompt else PromptStatus.TASK_PI_TEMPLATE

        # PI Data QA for session is processed at this point
        self.session_processed_dict[session_id] = True

        return [{'prompt_text': angix_prompt, 'prompt_status': prompt_status}]

    def session_processed(self, session_id):
        return self.session_processed_dict[session_id]
