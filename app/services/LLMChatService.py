import json
import os
import openai
import tiktoken

from app.utils.ChatPromptTemplates import SYSTEM_INITIATION_PROMPT


def num_tokens_from_messages(messages, model="gpt-3.5-turbo-0301"):
    encoding = tiktoken.encoding_for_model(model)
    num_tokens = 0
    for message in messages:
        num_tokens += 4  # every message follows <im_start>{role/name}\n{content}<im_end>\n
        for key, value in message.items():
            num_tokens += len(encoding.encode(value))
            if key == "name":  # if there's a name, the role is omitted
                num_tokens += -1  # role is always required and always 1 token
    num_tokens += 2  # every reply is primed with <im_start>assistant
    return num_tokens


def print_conversation(prompts):
    for item in prompts:
        print(f"[{item['role'].upper()}]")
        print(item['content'])
        print()


class LLMChatService:
    def __init__(self):
        self.LANDING_MAX_TOKEN = 100

        self.chatgpt_model_name = None
        self.fine_tuned_model_name = None
        self.temperature = 0.5
        self.max_response_tokens = 100
        self.top_p = 0.9
        self.frequency_penalty = 0
        self.presence_penalty = 0
        self.overall_max_tokens = 4096

        self.chat_session_dict = dict()
        self.prompt_array = []

    def load_open_ai_model_config(self, config_file_path):
        with open(config_file_path, 'r') as config_file:
            config_details = json.load(config_file)
        # Setting up the deployment name
        self.chatgpt_model_name = config_details['CHATGPT_MODEL']
        self.fine_tuned_model_name = config_details['FINE_TUNED_MODEL']
        openai.api_key = os.getenv("OPENAI_API_KEY")

    def initiate_chat_session(self, angix_session, config_file_path):
        self.load_open_ai_model_config(config_file_path)
        self.chat_session_dict[angix_session] = []
        self.add_session_prompt(angix_session, {"role": "system", "content": SYSTEM_INITIATION_PROMPT.strip()})
        self.post_session_prompts_to_chatgpt_model(angix_session, self.LANDING_MAX_TOKEN)

    def get_session_prompt_last(self, session_id):
        return self.prompt_array[self.chat_session_dict[session_id][-1]]

    def get_session_prompt_all(self, session_id):
        return [self.prompt_array[idx] for idx in self.chat_session_dict[session_id]]

    def add_session_prompt(self, session_id, prompt):
        self.prompt_array.append(prompt)
        self.chat_session_dict[session_id].append(len(self.prompt_array) - 1)

    # This function post whole session messages to OpenAi chat completion endpoint using chat gpt model
    def post_session_prompts_to_chatgpt_model(self, session_id, max_response_tokens):
        messages = self.get_session_prompt_all(session_id)
        token_count = num_tokens_from_messages(messages)

        # remove first message while over the token limit
        while token_count > self.overall_max_tokens - max_response_tokens:
            messages.pop(0)
            token_count = num_tokens_from_messages(messages)

        response = self.__call_openai_chat_completion(messages, max_response_tokens)

        self.add_session_prompt(session_id, {"role": "assistant", "content": response})
        print_conversation(self.get_session_prompt_all(session_id))

    # This is an internal method, so it should not be called outside this class
    def __call_openai_chat_completion(self, messages, max_response_tokens):
        response = openai.ChatCompletion.create(
            model=self.chatgpt_model_name,
            messages=messages,
            temperature=self.temperature,
            max_tokens=max_response_tokens,
            top_p=self.top_p,
            frequency_penalty=self.frequency_penalty,
            presence_penalty=self.presence_penalty,
        )
        return response['choices'][0]['message']['content'].strip()

    # This function post whole session prompts to OpenAi completion endpoint using our fine-tuned model
    def post_session_prompts_to_fine_tuned_model(self, session_id, max_response_tokens):
        prompt_array = [item['content'] for item in self.get_session_prompt_all(session_id)]
        token_count = num_tokens_from_messages(prompt_array)

        # remove first message while over the token limit
        while token_count > self.overall_max_tokens - max_response_tokens:
            prompt_array.pop(0)
            token_count = num_tokens_from_messages(prompt_array)

        prompt = "".join(prompt_array)

        response = self.__call_openai_completion(prompt, max_response_tokens)

        self.add_session_prompt(session_id, {"role": "assistant", "content": response})
        print_conversation(self.get_session_prompt_all(session_id))

    # This is an internal method, so it should not be called outside this class
    def __call_openai_completion(self, prompt, max_response_tokens):
        response = openai.Completion.create(
            model=self.fine_tuned_model_name,
            prompt=prompt,
            temperature=self.temperature,
            max_tokens=max_response_tokens,
            top_p=self.top_p,
            frequency_penalty=self.frequency_penalty,
            presence_penalty=self.presence_penalty
        )
        return response['choices'][0]['text'].strip()

    # Defining a function to print out the conversation in a readable format

