class ResultGenerationService:
    def process_angi_sr_interview(self):
        raise NotImplementedError

    def get_session_id(self):
        raise NotImplementedError

    def register_consumer(self):
        raise NotImplementedError

    def create_service_request(self):
        raise NotImplementedError

    def get_matching_pros_with_sr(self):
        raise NotImplementedError

    def get_matching_pros_without_sr(self):
        raise NotImplementedError

    def manage_results_generation(self, session_id, customer_prompt):
        raise NotImplementedError
