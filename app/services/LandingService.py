from app.utils.PromptStatus import PromptStatus


class LandingService:
    def __init__(self):
        self.session_processed_dict = dict()

    def process_customer_meta_data(self):
        raise NotImplementedError

    def generate_introduction_system_prompt(self):
        raise NotImplementedError

    def generate_introduction_angix_prompt(self):
        raise NotImplementedError

    def manage_landing(self, session_id, assistant_prompt):
        self.session_processed_dict[session_id] = True
        return [{'prompt_text': assistant_prompt['content'], 'prompt_status': PromptStatus.LANDING}]

    def session_processed(self, session_id):
        return self.session_processed_dict[session_id]
