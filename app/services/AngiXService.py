import os
from random import randrange

from app.services.LLMChatService import LLMChatService
from app.services.LandingService import LandingService
from app.services.PIDataQAService import PIDataQAService
from app.services.ResultsGenerationService import ResultGenerationService
from app.services.TaskDetectionService import TaskDetectionService
from app.services.TaskQAService import TaskQAService
from app.utils.PromptStatus import PromptStatus

from dotenv import load_dotenv
from werkzeug.exceptions import HTTPException

from app.utils.SessionStates import SessionState


class AngiXService:
    def __init__(self):
        load_dotenv()
        self.llm_chat_service = LLMChatService()
        self.landing_service = LandingService()
        self.task_detection_service = TaskDetectionService(self.llm_chat_service)
        self.task_qa_service = TaskQAService(self.llm_chat_service)
        self.pi_data_qa_service = PIDataQAService(self.llm_chat_service)
        self.results_generation_service = ResultGenerationService()
        self.session_state_dict = dict()
        self.resource_id_array = []
        self.prompt_array = []

    def initiate_session(self):
        try:
            # TODO Increase the range before release
            session_id = str(randrange(1))
            self.llm_chat_service.initiate_chat_session(session_id, os.path.join('config.json'))
            self.session_state_dict[session_id] = SessionState.LANDING
            last_assistant_prompt = self.llm_chat_service.get_session_prompt_last(session_id)
            resource_id = self.store_prompts(self.landing_service.manage_landing(session_id, last_assistant_prompt))
            return {'session_id': session_id, 'resource_id': resource_id}
        except Exception:
            raise HTTPException

    def get_session_prompt(self, resource_id):
        return {idx: self.prompt_array[prompt_id] for idx, prompt_id in enumerate(self.resource_id_array[resource_id])}

    def store_prompts(self, prompts):
        prompt_id_array = []
        for item in prompts:
            prompt_id_array.append(len(self.prompt_array))
            self.prompt_array.append(item)

        self.resource_id_array.append(prompt_id_array)
        return len(self.resource_id_array) - 1

    def manage_session_post(self, session_id, customer_prompt):
        session_state = self.session_state_dict[session_id]
        self.store_prompts([{'prompt_text': customer_prompt, 'prompt_status': PromptStatus.CUSTOMER_MESSAGE}])
        if session_state in [SessionState.INITIAL_TASK_DETECTION_QA, SessionState.FINAL_TASK_DETECTION_QA]:
            resource_id = self.store_prompts(self.task_detection_service.manage_task_detection(session_id, customer_prompt))
        elif session_state == SessionState.TASK_QA:
            resource_id = self.store_prompts(self.task_qa_service.manage_task_qa(session_id, customer_prompt))
        elif session_state == SessionState.PI_DATA_QA:
            resource_id = self.store_prompts(self.pi_data_qa_service.manage_pi_data_qa(session_id, customer_prompt))
        elif session_state == SessionState.RESULTS_GENERATION:
            resource_id = self.store_prompts(self.results_generation_service.manage_results_generation(session_id, customer_prompt))
        else:
            raise HTTPException

        self.set_session_state(session_id)

        return {'resource_id': resource_id}

    def set_session_state(self, session_id):
        session_state = self.session_state_dict[session_id]
        if session_state == SessionState.LANDING and self.landing_service.session_processed(session_id):
            self.session_state_dict[session_id] = SessionState.INITIAL_TASK_DETECTION_QA

        if session_state == SessionState.INITIAL_TASK_DETECTION_QA and self.task_detection_service.session_processed(session_id):
            self.session_state_dict[session_id] = SessionState.TASK_QA

        if session_state == SessionState.TASK_QA and self.task_qa_service.session_processed(session_id):
            self.session_state_dict[session_id] = SessionState.FINAL_TASK_DETECTION_QA

        if session_state == SessionState.FINAL_TASK_DETECTION_QA and self.task_detection_service.session_processed(session_id):
            self.session_state_dict[session_id] = SessionState.PI_DATA_QA

        if session_state == SessionState.PI_DATA_QA and self.pi_data_qa_service.session_processed(session_id):
            self.session_state_dict[session_id] = SessionState.RESULTS_GENERATION
