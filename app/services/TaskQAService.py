from app.utils.ChatPromptTemplates import TASK_DETECTION_MATCH_CHAT_TEMPLATE, DEFAULT_CUSTOMER_CHAT_TEMPLATE
import requests


class TaskQAService:
    def __init__(self, llm_llm_chat_service):
        self.TASK_QA_CUSTOMER_MAX_TOKEN = 0
        self.TASK_QA_FOLLOWUP_MAX_TOKEN = 500
        self.TASK_QA_ANSWER_MAX_TOKEN = 50
        self.TASK_QA_CONFIDENCE_MAX_TOKEN = 3

        self.session_processed_dict = dict()
        self.session_task_qa_dict = {}
        self.llm_chat_service = llm_llm_chat_service

    def initiate_session_task_qa(self, session_id, taskoid):
        self.session_task_qa_dict[session_id] = {'taskoid': taskoid,
                                                 'answered_questions': dict(),
                                                 'unanswered_questions': self.get_task_qa_sets(taskoid)}

    def get_task_qa_sets(self, taskoid):
        url = f"https://www.homeadvisor.com/api/resource/sr/qna/{taskoid}/interview/3/sets"
        response = requests.get(url)
        interviewSetId = response.json()['interviewSetId']

        url = f"https://www.homeadvisor.com/api/resource/sr/qna/sets/{interviewSetId}/interview"
        response = requests.get(url)
        return response.json()['questions']

    def transform_qa_to_chat_prompt(self, question):
        return f"{question['question']}\n1- {question['answer1']}\n2- {question['answer2']}"

    def process_task_qa(self, session_id, question):
        confidence_prompt = self.generate_confidence_chat_prompt(question)
        self.llm_chat_service.add_session_prompt(session_id, confidence_prompt)
        self.llm_chat_service.post_session_prompts_to_chatgpt_model(session_id, self.TASK_QA_CONFIDENCE_MAX_TOKEN)
        confidence = self.llm_chat_service.get_session_prompt_last(session_id)
        if confidence > 7:  # assuming 7 as a threshold for high confidence
            answer_prompt = self.generate_answer_chat_prompt(question)
            self.llm_chat_service.add_session_prompt(session_id, answer_prompt)
            self.llm_chat_service.post_session_prompts_to_chatgpt_model(session_id, self.TASK_QA_ANSWER_MAX_TOKEN)
            answer = self.llm_chat_service.get_session_prompt_last(session_id)
            question['answer'] = answer
        return question

    def process_task_qa_followup(self, session_id, question_list):
        unanswered_questions = [q for q in question_list if 'answer' not in q]
        followup_prompt = self.generate_followup_chat_prompt(unanswered_questions)
        self.llm_chat_service.add_session_prompt(session_id, followup_prompt)
        self.llm_chat_service.post_session_prompts_to_chatgpt_model(session_id, self.TASK_QA_FOLLOWUP_MAX_TOKEN)
        followup_questions = self.llm_chat_service.get_session_prompt_last(session_id)
        return followup_questions

    def generate_customer_chat_prompt(self, session_id, customer_prompt):
        prompt = DEFAULT_CUSTOMER_CHAT_TEMPLATE.substitute(customer_prompt=customer_prompt)
        self.llm_chat_service.add_session_prompt(session_id, {"role": "System", "content": prompt.strip()})

    def generate_confidence_chat_prompt(self, question):
        return {"role": "user",
                "content": f"What is your confidence answering the following question on scale of 1-10? 1 is the lowest and 10 is the highest\n{self.transform_qa_to_chat_prompt(question)}"}

    def generate_answer_chat_prompt(self, question):
        return {"role": "user",
                "content": f"What is the answer to this question? Just give the probability percentage for each choice\n{self.transform_qa_to_chat_prompt(question)}"}

    def generate_followup_chat_prompt(self, question_combined_string):
        return {"role": "user",
                "content": f"What are the minimal questions that you need to ask the consumer, so they can provide "
                           f"you with enough info to answer the following questions:\n{question_combined_string}"}

    def generate_qa_text_angix(self, chat_prompt):
        return chat_prompt['content']


    def generate_qa_template_angix(self, chat_prompt):
        # clean up chat gpt question and prepare it for AngiX to show it to customer as template -
        # it has a different tag from last one, check Prompt_Status
        cleaned_prompt = chat_prompt['content'].strip()  # remove leading and trailing whitespace

        filled_template = TASK_DETECTION_MATCH_CHAT_TEMPLATE.substitute(customer_prompt=cleaned_prompt)
        return filled_template

    def manage_task_qa(self, session_id, customer_prompt):
        # Initialize session processing
        if session_id not in self.session_processed_dict:
            self.session_processed_dict[session_id] = False

        if customer_prompt:
            self.generate_customer_chat_prompt(session_id, customer_prompt)
            self.llm_chat_service.post_session_prompts_to_chatgpt_model(session_id, self.TASK_QA_CUSTOMER_MAX_TOKEN)

        for question in self.session_task_qa_dict[session_id]['unanswered_questions']:
            updated_question = self.process_task_qa(session_id, question)
            if 'answer' in updated_question:
                self.session_task_qa_dict[session_id]['answered_questions'][updated_question['id']] = updated_question

        self.session_task_qa_dict[session_id]['unanswered_questions'] = [q for q in
                                                                         self.session_task_qa_dict[session_id][
                                                                             'unanswered_questions'] if
                                                                         'answer' not in q]

        angix_prompt = self.process_task_qa_followup(session_id, self.session_task_qa_dict[session_id]['unanswered_questions'])
        return angix_prompt

    def update_unanswered_questions(self, session_id, angix_prompt):
        # Assuming angix_prompt is a list of questions
        for question in angix_prompt:
            self.session_task_qa_dict[session_id]['unanswered_questions'].append(question)

    def get_answered_questions(self, session_id):
        return self.session_task_qa_dict[session_id]['answered_questions']

    def get_unanswered_questions(self, session_id):
        return self.session_task_qa_dict[session_id]['unanswered_questions']

    def get_category_qa_sets(self):
        raise NotImplementedError

    def session_processed(self, session_id):
        return self.session_processed_dict[session_id]


