from flask import Flask
from .controllers.AngiXController import angi_x


def create_app():
    app = Flask(__name__)

    # Register blueprints
    app.register_blueprint(angi_x)

    return app
