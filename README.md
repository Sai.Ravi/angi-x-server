# Angi X Server



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

# Running the Application locally

## Option 1: Running Entire FE and BE Locally with Docker Compose
### Setting up the .env File and Docker Compose

To successfully run this Next.js repository, you will need to set up your `.env` file and run Docker Compose. Follow the steps below to get started:

### Setting up the .env file

1. Create a `.env` file in the root directory of your project.
2. Add the following two variables to your `.env` file:

```
GIT_TOKEN=<git_token>
OPENAI_API_KEY=<open_api_key>
```

Replace `<git_token>` with your Git token and `<open_api_key>` with your OpenAI API key.

#### Obtaining the OpenAI API Key

1. To generate an OpenAI API key, you will need to log into OpenAI using the information mentioned in the following Slack message: [OpenAI API Info](https://angi.slack.com/archives/C05AUQ1LJ8K/p1686324838372889).
2. Log in using your username and password.
3. Once logged in, click the Angi, Inc. icon in the top-right corner of the screen, then select "View API Keys".
4. Click on "Generate Secret Key" to generate a new key.
5. Copy the generated key and paste it as `<open_api_key>` in your `.env` file.

#### Obtaining the Git Token

1. In GitLab, click on your profile icon in the top-left corner, then select "Edit Profile".
2. In the left-hand menu, click on "Access Tokens".
3. Provide a name for the access token, set an expiration date (a week after the current date), select "API" as the scope, and click on "Create Personal Access Token".
4. Save the generated access token and paste it as `<git_token>` in your `.env` file.

### Running the Application

To run the Docker Compose file and start both the back-end and front-end services, follow these steps:

1. Open your terminal and navigate to the root directory of your project.
2. Run the command `docker-compose up` in the terminal.
3. Both the back-end and front-end services should start up successfully.
4. Open your browser and visit http://localhost:3000 to see the front-end application.
5. To test an endpoint, you can use the `curl` command in your terminal. For example, you can run `curl http://localhost:5001/api/session` to get a JSON object containing the session ID. If you have the `jq` tool installed, you can use `curl http://localhost:5001/api/session | jq` to format the JSON response.
6. To shut down the project, run the command `docker-compose down` in your terminal.

## Option 2: Running the Backend Application Locally with Dockerfile

To run only the backend (BE) application locally using the Dockerfile, follow the steps below:

1. Open your terminal.
2. Build the Docker image by running the command `docker build -t angi-x-server .` in the terminal. This will create the Docker image for your backend application.
3. After the image is successfully built, start the application by running the command `docker run -p 5001:5000 angi-x-server`. This will run a container based on the image and expose port 5001 for the backend application.
4. To test an endpoint, you can use the `curl` command in your terminal. For example, you can run `curl http://localhost:5001/api/session` to get a JSON object containing the session ID. If you have the `jq` tool installed, you can use `curl http://localhost:5001/api/session | jq` to format the JSON response.
5. To stop the container, first run the command `docker ps` to get the container ID of the running container.
6. Copy the container ID and then run the command `docker stop <container_id>` to stop the container.

Make sure to replace `<container_id>` with the actual container ID obtained from the `docker ps` command.

Please note that running the backend application with Docker requires Docker to be installed and properly configured on your machine.

